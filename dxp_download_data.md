Here, you will find an overview of the open source informmation of this product. The detailed information can be found within the repository at [GitLab](https://gitlab.com/ourplant.net/products/s3-0001-tool-changing-unit-tcu-2).

|document|download options|
|:-----|-----:|
|operating manual           |[de](https://gitlab.com/ourplant.net/products/s3-0001-tool-changing-unit-tcu-2/-/raw/main/01_operating_manual/S3-0001_B_Betriebsanleitung.pdf), [en](https://gitlab.com/ourplant.net/products/s3-0001-tool-changing-unit-tcu-2/-/raw/main/01_operating_manual/S3-0001_A_Operating%20Manual.pdf/)|
|assembly drawing           |[de](https://gitlab.com/ourplant.net/products/s3-0001-tool-changing-unit-tcu-2/-/raw/main/02_assembly_drawing/s3-0001_C_ZNB_TCU-2.PDF)|
|circuit diagram            |[de](https://gitlab.com/ourplant.net/products/s3-0001-tool-changing-unit-tcu-2/-/raw/main/03_circuit_diagram/S3-0001-EPLAN-C.pdf)|
|maintenance instructions   ||
|spare parts                |[de](https://gitlab.com/ourplant.net/products/s3-0001-tool-changing-unit-tcu-2/-/raw/main/05_spare_parts/S3-0001_B_EVL.pdf), [en](https://gitlab.com/ourplant.net/products/s3-0001-tool-changing-unit-tcu-2/-/raw/main/05_spare_parts/S3-0001_A_EVL_engl.pdf)|

<!-- 2021 (C) Häcker Automation GmbH -->
