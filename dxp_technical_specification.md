The S3-0001 tool changing unit has the following technical specifications.

## Technical information
|Parameter    | Value|
| :--------------------------- | -------------- |
| Dimensions in mm (W x D x H) | 49 x 224 x 514 |
| Weight in kg                 | n.v.             |
| Movement range in Z in mm    | n.v.          |
| Voltage in V                 | 24             |
| Max. current in A            | 1             |
| Communication interface      | UNICAN         |
