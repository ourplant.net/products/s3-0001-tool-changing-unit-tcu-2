The tool changing unit enables the time-saving and flexible conversion of tools during the machining process. The device has the function of a magazine that provides tools for automatic change.
